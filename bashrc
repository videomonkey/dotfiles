# If not running interactively, don't do anything
[[ $- != *i* ]] && return

stty -ixon # Disables ctrl-s and ctrl-q (Used To Pause Term)

# Shell History Is Your Best Productivity Tool
# https://martinheinz.dev/blog/110
HISTCONTROL=ignoreboth                      # don't put duplicate lines or lines starting with space in the history.
shopt -s histappend                         # append to the history file, don't overwrite it
PROMPT_COMMAND="history -a;$PROMPT_COMMAND" # write history out immediately https://askubuntu.com/questions/67283/is-it-possible-to-make-writing-to-bash-history-immediate
HISTSIZE=10000000
HISTFILESIZE=10000000
# https://unix.stackexchange.com/questions/32460/excluding-some-of-the-commands-from-being-getting-stored-in-bash-history
HISTIGNORE='history*:exit*:ls:ll:eza:clear:x'

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm* | rxvt*)
	PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
	;;
*) ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias dir='dir --color=auto'
	alias vdir='vdir --color=auto'
	alias grep='grep --color=auto'
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

# My Edits
VAGRANT_DEFAULT_PROVIDER=libvirt
export PATH=$PATH:/usr/local/go/bin
export PATH=~/.local/bin:$PATH
export PATH=~/.local/bin/platform-tools/:$PATH
export PATH="~/Applications/:$PATH"
export EDITOR=nvim
export CDPATH=$HOME:..
# from https://blog.meain.io/2023/navigating-around-in-shell/
#shopt nocaseglob # ignore case when matching
#shopt cdspell    # fix common spelling mistakes
shopt -s autocd # if the text isn't a binary and is a folder, go there

# some aliases stolen from https://github.com/drewgrif/jag_dots/blob/main/.bashrc
alias install='sudo apt install'
alias update='sudo apt update'
alias upgrade='sudo apt upgrade'
alias uplist='apt list --upgradable'
alias remove='sudo apt autoremove'
alias rcedit="$EDITOR $HOME/.bashrc"
alias nethogs="/usr/sbin/nethogs"
alias iftop="/usr/sbin/iftop"
alias l='eza -ll --color=always --group-directories-first'
alias ll="eza -la --git --icons --group-directories-first"
alias ls='eza --git --icons --group-directories-first'
alias df='df -h'
alias reload='source ~/.bashrc'
alias dirs='dirs -v'
alias slurp='pushd - && pushd'
alias g.="cd ~/.config"
alias gd="cd ~/Downloads"
alias free='free -h'
alias myip="ip -f inet address | grep inet | grep -v 'lo$' | cut -d ' ' -f 6,13 && curl ifconfig.me && echo ' external ip'"
alias x="exit"
alias cat=batcat
#alias df=duf
# this is where gnome mounts cameras and user network drives
alias volumes="cd $XDG_RUNTIME_DIR/gvfs"
#alias sudo="sudo env PATH=$PATH" # keeps my personal binaries and paths in the sudo path

# filters history arrow searching based on text input
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

# https://fedoramagazine.org/install-powershell-on-fedora-linux/
alias pwsh="podman run -it --rm --privileged --env-host --net=host --pid=host --ipc=host --volume $HOME:$HOME --volume /:/var/host mcr.microsoft.com/powershell"

export CLICOLOR=1

# don't forget to use nala instead of apt.
# don't foget `ncdu` for disk usage
# [ ] get eza, apparently it's better than exa
# play with yazi file browser
# don't foget `mc` for file management

# Adding the pbcopy and pbppaste commands from macos
# https://ostechnix.com/how-to-use-pbcopy-and-pbpaste-commands-on-linux/
alias pbcopy='xsel --clipboard --input'
alias pbpaste='xsel --clipboard --output'

# btop is provided by an ubuntu distrobox
alias btop='btop --utf-force'

# ================= FZF ===============
# this makes fzf the search engine for ctrl-r,
# and also enables the alt-c and ctrl-t shortcuts
[ -f /usr/share/doc/fzf/examples/key-bindings.bash ] && source /usr/share/doc/fzf/examples/key-bindings.bash
# https://ubuntuforums.org/showthread.php?t=2474442
# apparently this needs to go last
[ -f /usr/share/bash-completion/completions/fzf ] && source /usr/share/bash-completion/completions/fzf
# this is supposed to be automatically lazy loaded after trying
# the command `fzf **<tab>` I've been having problems with that.
# `apt show fzf` will point to a readme. `dpkg -S fzf | grep 'bindings\|completion'`
# will list out files related to bings and completions that were installed
# https://github.com/junegunn/fzf/issues/1119
# https://bugs.debian.org/cgi-bin/pkgreport.cgi?dist=unstable;package=FZF
# https://github.com/scop/bash-completion/issues/1055
# https://github.com/junegunn/fzf/issues/3457
# https://github.com/lincheney/fzf-tab-completion?tab=readme-ov-file#bash
[ -f $HOME/.cargo/env ] && . "$HOME/.cargo/env"
eval "$(starship init bash)"
deno() {
	podman run \
		--interactive \
		--tty \
		--rm \
		--network=host \
		--volume $PWD:/app \
		--volume $HOME/.deno:/deno-dir \
		--workdir /app \
		denoland/deno:2.1.3 \
		"$@"
}
alias node='podman run -it --rm --network=host --volume $PWD:/app --workdir /app docker.io/library/node:lts-bookworm node'
alias nodejs=node
alias npx='podman run -it --rm --network=host --volume $PWD:/app --workdir /app docker.io/library/node:lts-bookworm npx'

function y() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(command cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		builtin cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}
export LIBVIRT_DEFAULT_URI='qemu:///system'
