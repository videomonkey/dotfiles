-- `gf` on a file path will go to the files
-- https://github.com/VonHeikemen/nvim-starter/blob/01-base/init.lua

-- Shortcuts
--vim.keymap.set({'n', 'x', 'o'}, '<leader>h', '^')
--vim.keymap.set({'n', 'x', 'o'}, '<leader>l', 'g_')
--vim.keymap.set('n', '<leader>a', ':keepjumps normal! ggVG<cr>')

-- Basic clipboard interaction
--vim.keymap.set({'n', 'x'}, 'gy', '"+y') -- copy
--vim.keymap.set({'n', 'x'}, 'gp', '"+p') -- paste

-- Delete text
--vim.keymap.set({'n', 'x'}, 'x', '"_x')
--vim.keymap.set({'n', 'x'}, 'X', '"_d')

-- Commands
--vim.keymap.set('n', '<leader>w', '<cmd>write<cr>')
--vim.keymap.set('n', '<leader>bq', '<cmd>bdelete<cr>')
--vim.keymap.set('n', '<leader>bl', '<cmd>buffer #<cr>')

-- ========================================================================== --
-- ==                               COMMANDS                               == --
-- ========================================================================== --

--local group = vim.api.nvim_create_augroup('user_cmds', {clear = true})

--vim.api.nvim_create_autocmd('TextYankPost', {
--  desc = 'Highlight on yank',
--  group = group,
--  callback = function()
--    vim.highlight.on_yank({higroup = 'Visual', timeout = 200})
--  end,
--})

--vim.api.nvim_create_autocmd('FileType', {
--  pattern = {'help', 'man'},
--  group = group,
--  command = 'nnoremap <buffer> q <cmd>quit<cr>'
--})

-- ========================================================================== --
-- ==                               PLUGINS                                == --
-- ========================================================================== --

-- find them in ~/.config/nvim/lua/plugins/*.lua
-- learn about configuring them at https://www.lazyvim.org/configuration/plugins

-- bootstrap lazy.nvim, LazyVim and your plugins
require("config.lazy")

-- ========================================================================== --
-- ==                         PLUGIN CONFIGURATION                         == --
-- ========================================================================== --

---
-- Colorscheme
---

---
-- lualine.nvim (statusline)
---
require("lualine").setup({
  options = {
    icons_enabled = false,
    theme = "dracula",
    component_separators = "|",
    section_separators = "",
  },
})

--require("zen-mode").setup({
--  window = {
--    -- backdrop = 0.95, -- shade the backdrop of the Zen window. Set to 1 to keep the same as Normal
--    -- height and width can be:
--    -- * an absolute number of cells when > 1
--    -- * a percentage of the width / height of the editor when <= 1
--    -- * a function that returns the width or the height
--    width = 0.66, -- width of the Zen window
--    height = 0.66, -- height of the Zen window
--    -- by default, no options are changed for the Zen window
--    -- uncomment any of the options below, or add other vim.wo options you want to apply
--    options = {
--      -- signcolumn = "no", -- disable signcolumn
--      number = false, -- disable number column
--      relativenumber = false, -- disable relative numbers
--      cursorline = false, -- disable cursorline
--      cursorcolumn = false, -- disable cursor column
--      foldcolumn = "0", -- disable fold column
--      -- list = false, -- disable whitespace characters
--    },
--  },
--  plugins = {
--    vim.opt.colorcolumn = 0
--    options = {
--      enabled = true,
--      -- ruler = false, -- disables the ruler text in the cmd line area
--      showcmd = false, -- disables the command in the last line of the screen
--      -- you may turn on/off statusline in zen mode by setting 'laststatus'
--      -- statusline will be shown only if 'laststatus' == 3
--      laststatus = 0, -- turn off the statusline in zen mode
--    },
--    twilight = { enabled = true }, -- enable to start Twilight when zen mode opens
--    gitsigns = { enabled = false }, -- disables git signs
--    tmux = { enabled = false }, -- disables the tmux statusline
--    -- this will change the font size on alacritty when in zen mode
--    -- requires  Alacritty Version 0.10.0 or higher
--    -- uses `alacritty msg` subcommand to change font size
--    alacritty = {
--      -- enabled = true,
--      -- font = "20", -- font size
--    },
--  },
--  -- callback where you can add custom code when the Zen window opens
--  on_open = function(win) end,
--  -- callback where you can add custom code when the Zen window closes
--  on_close = function() end,
--})
