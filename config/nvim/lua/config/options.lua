-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

-- so that single file podman binds don't break
vim.opt.backupcopy = "yes"

-- Always keep 8 lines above/below cursor unless at start/end of file
vim.opt.scrolloff = 8

-- Place a column line
vim.opt.colorcolumn = "80"

vim.opt.number = false
vim.opt.relativenumber = false
vim.opt.breakindent = true
vim.opt.wrap = true -- line wrap
vim.opt.showmode = false
