return {
  { "catppuccin/nvim", name = "catppuccin" },
  { "rose-pine/neovim", name = "rose-pine" },
  { "Mofiqul/dracula.nvim", priority = 1000 },
  { "junegunn/goyo.vim", enabled = false },
  -- Configure LazyVim to load gruvbox
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "dracula",
    },
  },
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function()
      vim.fn["mkdp#util#install"]()
    end,
  },
  {
    "folke/zen-mode.nvim",
    opts = {
      --window.width = 80,
      --window.options.number = false,
      --window.options.relativenumber = false,
      --window.options.cursorcolumn = false,
    },
  },
}
