# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="robbyrussell"

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git wp-cli rvm)

source $ZSH/oh-my-zsh.sh

# https://code-examples.net/en/q/1f5fc9
# if there is an mvim executeable, use that instead of vim
if [[ -x /Applications/MacVim.app/Contents/bin/mvim ]]; then
	alias mvim=/Applications/MacVim.app/Contents/bin/mvim
	alias vim="mvim -v"
	alias vi="mvim -v"
fi
export EDITOR='vim'

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

alias l="exa -alh --git || ls -Alh"
path+="$HOME/.local/bin" #ubuntu, adding pip3 stuff (or maybe just the dotfiles package) to the commandline.
path+="$HOME/Library/Python/3.7/bin" #ubuntu, adding pip3 stuff (or maybe just the dotfiles package) to the commandline.
path+='/snap/bin' #ubuntu, adding the ability to use snaps
#[TODO] I want to add git info to the right prompt
#[TODO] I would like separate my ubuntu specific stuff to a separate file that only loads in ubuntu
export MOSH_SERVER_SIGNAL_TMOUT=300
alias kill-mosh-clients="pkill -SIGUSER1 mosh-server"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"


export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

# https://dev.to/iggredible/how-to-search-faster-in-vim-with-fzf-vim-36ko
if type rg &> /dev/null; then
	export FZF_DEFAULT_COMMAND='rg --files'
	export FZF_DEFAULT_OPTS='-m --height 50% --border'
fi

if [ -x "$(command -v zoxide)" ]; then
	eval "$(zoxide init zsh)"
else
	echo "Info: zoxide not found"
fi

if [ -f ~/.tokens ]; then
	source ~/.tokens
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Generated for envman. Do not edit.
[ -s "$HOME/.config/envman/load.sh" ] && source "$HOME/.config/envman/load.sh"
export PATH="/usr/local/sbin:$PATH"
